# -*- coding: utf-8 -*-
"""
Created on Tue Sep  5 22:14:56 2023

@author: Dell
"""

# -*- coding: utf-8 -*-

# Feature Extraction with PCA
import numpy as np
from pandas import read_csv
from sklearn.decomposition import PCA



# Import libraries and classes required for this example:
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
# native
import os
import csv
import re
import tldextract

from sklearn.model_selection import cross_validate
from sklearn.metrics import make_scorer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold

from sklearn.decomposition import PCA

#feature selection
from numpy import set_printoptions
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import RFE
from newspaper import Article
from sklearn.metrics import roc_auc_score


import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import matplotlib.pyplot as plt
import re
import seaborn as sns


from urllib.parse import urlparse


names = ['url', 'fqdn_length', 'fqdn_digit_percentage','unique_length',
         'dot_count','hyphen_count','count?','count_dir','use_of_ip','short_url',
         'sub_domain_count','type','rank']

# taking dataset from benign dataset
dataset = pd.read_csv("./features_benign.csv",names=names,skiprows=1) 
group_by_diag = dataset.groupby("type").count().reset_index()
sizes = group_by_diag['fqdn_length']
labels = group_by_diag['type']
colors = ["#1f77b4", "#ff7f0e"]


plt.pie(sizes, labels = ['Benign','Malicious'], colors = colors,autopct='%1.2f%%')
plt.show()

# set width of bar
barWidth = 0.25
fig = plt.subplots(figsize =(12, 8))
 




plt.figure(figsize=(20,5))
plt.hist(dataset['fqdn_length'],bins=50,color='LightBlue')
plt.title("URL-Length",fontsize=20)
plt.xlabel("Url-Length",fontsize=18)
plt.ylabel("Number Of Urls",fontsize=18)
plt.ylim(0,1000)


plt.figure(figsize=(15,5))
plt.title("Number Of Directories In Url",fontsize=20)
sns.countplot(x='count_dir',data=dataset,hue='type')
plt.xlabel("Number Of Directories",fontsize=18)
plt.ylabel("Number Of URLs",fontsize=18)




plt.figure(figsize=(15,5))
plt.title("Use Of IP In Url",fontsize=20)
plt.xlabel("Use Of IP",fontsize=18)
sns.countplot(dataset['use_of_ip'])
plt.ylabel("Number of URLs",fontsize=18)


plt.figure(figsize=(15,5))
plt.title("Use Of IP In Url",fontsize=20)
plt.xlabel("Use Of IP",fontsize=18)
sns.countplot(dataset['tld_suffix'])
plt.ylabel("Number of URLs",fontsize=18)



 
# creating dataframe
df = pd.DataFrame({
    'Name': ['Imbalanced  ', 'Balanced ' ],
    
    'SVM': [95.83,99.42],
    'KNN': [99.78,99.85],
    
    'Logisctic': [99.81, 99.89],
    'Random Forest': [99.96, 99.96]
})

 

# plotting graph
test= df.plot(x="Name", y=["SVM","KNN", "Logisctic","Random Forest"], kind="bar")
plt.xlabel("Classification Model")
plt.ylabel("Accuracy")

 
plt.grid(True)
plt.show()

# creating dataframe
df = pd.DataFrame({
    'Name': ['Imbalanced  ', 'Balanced ' ],
    
    'SVM': [94.24,98.89],
    'KNN': [99.83,99.87],
    
    'Logisctic': [99.87, 99.72],
    'Random Forest': [99.97, 99.97]
})

# plotting graph
test= df.plot(x="Name", y=["SVM","KNN", "Logisctic","Random Forest"], kind="bar",
              color=['#C94845', '#4958B5', '#49D845', '#777777'])
plt.xlabel("Classification Model")
plt.ylabel("Precision")

 
plt.grid(True)
plt.show()


# creating dataframe
df = pd.DataFrame({
    'Name': ['Imbalanced  ', 'Balanced ' ],
    
    'SVM': [93.83,99.23],
    'KNN': [99.68,99.78],
    
    'Logisctic': [99.89, 99.84],
    'Random Forest': [99.95, 99.95]
})

# plotting graph
test= df.plot(x="Name", y=["SVM","KNN", "Logisctic","Random Forest"], kind="bar" ,
              color=['#FF7F50', '#40E0D0', '#49D845', '#CCCCFF'])
plt.xlabel("Classification Model")
plt.ylabel("Recall")

 
plt.grid(True)
plt.show()





# creating dataframe
df = pd.DataFrame({
    'Name': ['Imbalanced  ', 'Balanced ' ],
    
    'SVM': [97.03,99.45],
    'KNN': [99.75,99.83],
    
    'Logisctic': [99.88, 99.78],
    'Random Forest': [99.97, 99.97]
})

# plotting graph
test= df.plot(x="Name", y=["SVM","KNN", "Logisctic","Random Forest"], kind="bar",
              color=['#FFBF00', '#6495ED', '#49D845', '#40E0D0'])
plt.xlabel("Classification Model")
plt.ylabel("F1-Score")

 
plt.grid(True)
plt.show()



df = pd.DataFrame({
    'Name': ['Benign  ', 'Malcious ' ],
    
    'SVM': [74,109],
    'KNN': [75,108],
    
    'Logisctic': [80,103],
    'Random Forest': [63,120]
})

# plotting graph
test= df.plot(x="Name", y=["SVM","KNN", "Logisctic","Random Forest"], kind="bar",
              color=['#2eb135', '#4958B5', '#b55ef1', '#dd0088'])
plt.xlabel("Classification Model on Unknown Dataset")
plt.ylabel("Number of Urls ")

 
plt.grid(True)
plt.show()






