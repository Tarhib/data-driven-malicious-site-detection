# -*- coding: utf-8 -*-

# Feature Extraction with PCA
import numpy
from pandas import read_csv
from sklearn.decomposition import PCA



# Import libraries and classes required for this example:
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
# native
import os
import csv
import re
import tldextract

from sklearn.model_selection import cross_validate
from sklearn.metrics import make_scorer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold

from sklearn.decomposition import PCA

#feature selection
from numpy import set_printoptions
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import RFE
from newspaper import Article
from sklearn.metrics import roc_auc_score


import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import matplotlib.pyplot as plt
import re



from urllib.parse import urlparse


#DIR = os.path.dirname(__file__)

# Import dataset:
    
#dataset = pd.read_csv("./benign_domain_list.csv")




# Assign column names to dataset:
names = ['url', 'fqdn_length', 'fqdn_digit_percentage','digit_string_len','unique_length',
         'dot_count','hyphen_count','count?','count_dir','use_of_ip','short_url',
         'tld_suffix','sub_domain_count','type']

# taking dataset from benign dataset
dataset = pd.read_csv("./benign_domain_list.csv",names=names) 
dataset['type']=0

malicious_dataset = pd.read_csv("./Malicious_URLs-dataset-for-research.csv",names=names) 
malicious_dataset['type']=1

dataset = pd.concat([dataset, malicious_dataset], ignore_index=True)



    


dataset['tld_suffix']=''
dataset['fqdn_length']= dataset['url'].str.len();


dataset['digit_string']=dataset['url'].str.extract('(\d+)')
dataset['digit_string_len']=dataset['digit_string'].str.len()
dataset['fqdn_digit_percentage']= (dataset['digit_string_len']/dataset['fqdn_length'])*100
dataset['unique_length']=dataset['url'].apply(set).apply(len)
dataset['dot_count']= dataset['url'].str.count('\.')
dataset['hyphen_count']= dataset['url'].str.count('\-')

dataset['count?'] = dataset['url'].apply(lambda i: i.count('?'))

def no_of_dir(url):
    urldir = urlparse(url).path
    return urldir.count('/')
dataset['count_dir'] = dataset['url'].apply(lambda i: no_of_dir(i))




#Use of IP or not in domain
def having_ip_address(url):
    match_found = re.search(
        '(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.'
        '([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\/)|'  # IPv4
        '((0x[0-9a-fA-F]{1,2})\\.(0x[0-9a-fA-F]{1,2})\\.(0x[0-9a-fA-F]{1,2})\\.(0x[0-9a-fA-F]{1,2})\\/)' # IPv4 in hexadecimal
        '(?:[a-fA-F0-9]{1,4}:){7}[a-fA-F0-9]{1,4}', url)  # Ipv6
    if match_found:
        # print match.group()
        return -1
    else:
        # print 'No matching pattern found'
        return 1
dataset['use_of_ip'] = dataset['url'].apply(lambda i: having_ip_address(i))

def shortening_service(url):
    match_found = re.search('bit\.ly|goo\.gl|shorte\.st|go2l\.ink|x\.co|ow\.ly|t\.co|tinyurl|tr\.im|is\.gd|cli\.gs|'
                      'yfrog\.com|migre\.me|ff\.im|tiny\.cc|url4\.eu|twit\.ac|su\.pr|twurl\.nl|snipurl\.com|'
                      'short\.to|BudURL\.com|ping\.fm|post\.ly|Just\.as|bkite\.com|snipr\.com|fic\.kr|loopt\.us|'
                      'doiop\.com|short\.ie|kl\.am|wp\.me|rubyurl\.com|om\.ly|to\.ly|bit\.do|t\.co|lnkd\.in|'
                      'db\.tt|qr\.ae|adf\.ly|goo\.gl|bitly\.com|cur\.lv|tinyurl\.com|ow\.ly|bit\.ly|ity\.im|'
                      'q\.gs|is\.gd|po\.st|bc\.vc|twitthis\.com|u\.to|j\.mp|buzurl\.com|cutt\.us|u\.bb|yourls\.org|'
                      'x\.co|prettylinkpro\.com|scrnch\.me|filoops\.info|vzturl\.com|qr\.net|1url\.com|tweez\.me|v\.gd|'
                      'tr\.im|link\.zip\.net',
                      url)
    if match_found:
        return -1
    else:
        return 1
dataset['short_url'] = dataset['url'].apply(lambda i: shortening_service(i))





for index, row in dataset.iterrows():
   
    print(index)
    temp = tldextract.extract( row['url'] ).suffix
    subdoomain= tldextract.extract( row['url'] ).subdomain
    #dataset.at[index,'tld-suffix'] = tldextract.extract( row['url'] ).suffix
    dataset.at[index,'sub_domain_count'] = subdoomain.count('\.')+1
    
    
    ''''
    trying with request for redirect count
    print(row['tld-suffix'])
    rowdomain = tldextract.extract( row['url'] ).domain
    print( row['url'])
    url ='http://'+row['url'] 
    print(rowdomain)
    toi_article = Article( url, language="en") # en for English
    
    
    try:
        #To download the article
        toi_article.download()
         
        #To parse the article
        toi_article.parse()
         
        #To perform natural language processing ie..nlp
        toi_article.nlp()
        print(toi_article.keywords)
        
        if rowdomain in toi_article.keywords:
            dataset.at[index,'keyward-match'] = '1'
        else:
            dataset.at[index,'keyward-match'] = '0'
    except:
        print("Exception request")
        dataset.at[index,'keyward-match'] = '-1'
    
   
    try:
        r = requests.get('http://'+row['url'] ,timeout=5)
        dataset.at[index,'redirect-count'] = len(r.history)
        r.close()
    except requests.exceptions.RequestException as errex:
        print("Exception request")
        #dataset.at[index,'redirect-count'] = '-1'

    #dataset.at[index,'redirect-count'] = len(r.history)
    
    #row['tld-suffix'] =tldextract.extract( row['url'] ).suffix
    '''

#dataset.to_csv('features_benign.csv', index=False)

names2 = ['rank', 'url']
dataset2 = pd.read_csv("./tranco_full_list_for_ranking.csv",names=names2) 

dataset = pd.merge(dataset, dataset2,how='left')


dataset.drop(
    columns=["digit_string_len", "digit_string","tld_suffix"], 
    inplace=True)


'''
group_by_diag = dataset.groupby("type").count().reset_index()
sizes = group_by_diag['fqdn-length']
labels = group_by_diag['type']
colors = ["#1f77b4", "#ff7f0e"]


plt.pie(sizes, labels = ['Benign','Malicious'], colors = colors,autopct='%1.2f%%')
plt.show()
'''


dataset = dataset.fillna(0)

dataset.to_csv('features_benign.csv', header=True, index=False)









columns_titles= ['url','fqdn_length'	,'fqdn_digit_percentage','unique_length'	
            'dot_count','hyphen_count',	'count?	','count_dir','use_of_ip',	
            'short_url','sub_domain_count',	'rank','type']

dataset=dataset.reindex(columns=columns_titles)



dataset.head() 
# Assign values to the X and y variables:
X = dataset.iloc[:, 1:11].values
y = dataset.iloc[:, 11].values 

# Split dataset into random train and test subsets:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20) 

# Standardize features by removing mean and scaling to unit variance:
scaler = StandardScaler()
scaler.fit(X_train)

X_train_features = scaler.transform(X_train)
X_test_features = scaler.transform(X_test) 



# import SMOTE module from imblearn library
# pip install imblearn (if you don't have imblearn in your system)
from imblearn.over_sampling import SMOTE
sm = SMOTE(random_state = 2)
X_train_res, y_train_res = sm.fit_sample(X_train_features, y_train)



#Feature importance

'''
# feature extraction
#Univariate Selection
# feature extraction
pca = PCA(n_components=7)
fit = pca.fit(X)
# summarize components
#print("Explained Variance: %s" % fit.explained_variance_ratio_)
#print(fit.components_)
X_train_features = fit.transform(X_train)
X_test_features = fit.transform(X_test)
# summarize selected features
#print(features[0:5,:])

'''

from sklearn.metrics import confusion_matrix,classification_report,accuracy_score,f1_score,fbeta_score,make_scorer,precision_score,recall_score 

# creating a dictionary to evaluate metric over stratified k-fold cv
scoring = {'accuracy' : make_scorer(accuracy_score), 
           'precision' : make_scorer(precision_score),
           'recall' : make_scorer(recall_score), 
           'f1_score' : make_scorer(f1_score)}

#SVM Classification




from sklearn.svm import LinearSVC
svm=LinearSVC(C=0.0001)
svm.fit(X_train_features, y_train)

svm_scorar  = cross_validate(svm,X_test_features,y_test,cv=StratifiedKFold(n_splits=5,shuffle=True,random_state=111),scoring=scoring) 
print(f" ACCURACY: {svm_scorar['test_accuracy'].mean()}")
print(f" PRECISION: {svm_scorar['test_precision'].mean()}")
print(f" RECALL: {svm_scorar['test_recall'].mean()}")
print(f" F-1 Score: {svm_scorar['test_f1_score'].mean()}")

# Predict y data with classifier: 
y_predict = svm.predict(X_test_features)
print("SVM Classification Score Analysis")
svm_accuracy=accuracy_score(y_test, y_predict)
print(svm_accuracy)
print(roc_auc_score(y_test, y_predict))
print(classification_report(y_test, y_predict,digits=4))
print(confusion_matrix(y_test,y_predict))



#adding Ranodm Sample Classifcation for imabalanced data

from sklearn.ensemble import RandomForestClassifier # importing the function
rf = RandomForestClassifier(n_estimators=100,criterion='entropy',random_state=42,max_depth=5) # creating an instance
rf.fit(X_train_features,y_train)
rf_cv_f1 = cross_validate(rf,X_test_features,y_test,cv=StratifiedKFold(n_splits=5,shuffle=True,random_state=111),scoring=scoring) 
 # fitting the model
print(f" ACCURACY: {rf_cv_f1['test_accuracy'].mean()}")
print(f" PRECISION: {rf_cv_f1['test_precision'].mean()}")
print(f" RECALL: {rf_cv_f1['test_recall'].mean()}")
print(f" F-1 Score: {rf_cv_f1['test_f1_score'].mean()}")


y_predict = rf.predict(X_test_features) # predicting on the hold out test set
print(classification_report(y_test,y_predict,digits=4)) 
print(confusion_matrix(y_test,y_predict))




# Use the KNN classifier to fit data:
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(X_train_features, y_train) 
# Predict y data with classifier: 
y_predict = classifier.predict(X_test_features)
#print("train shape: " + str(X_train.shape))
#print("score on test: " + str(classifier.score(X_test, y_predict)))
#print("score on train: "+ str(classifier.score(X_train, y_train)))
print("KNN Classification Score Analysis")
knn_accuracy= accuracy_score(y_test, y_predict)
print(knn_accuracy)
roc_auc_score(y_test, y_predict)
print(roc_auc_score(y_test, y_predict))
print(classification_report(y_test, y_predict,digits=4))

#Decision Tree Classifcation

from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()
clf.fit(X_train_features, y_train)
# Predict y data with classifier: 
y_predict = clf.predict(X_test_features)
print("Decision Tree Score Analysis")

decision_tree_accuracy = accuracy_score(y_test, y_predict)

print(decision_tree_accuracy)
roc_auc_score(y_test, y_predict)
print(roc_auc_score(y_test, y_predict))
print(classification_report(y_test, y_predict,digits=4))


# Assign column names to dataset:
names_unknown = ['url', 'fqdn-length', 'fqdn-digit-percentage','digit-string-len','unique-length',
         'dot-count','hyphen-count','tld-suffix','sub-domain-count','type']

unknown_dataset = pd.read_csv("./mixed_domain_list.csv",names=names_unknown) 
unknown_dataset['fqdn-length']= unknown_dataset['url'].str.len();


unknown_dataset['digit-string']=unknown_dataset['url'].str.extract('(\d+)')
unknown_dataset['digit-string-len']=unknown_dataset['digit-string'].str.len()
unknown_dataset['fqdn-digit-percentage']= (unknown_dataset['digit-string-len']/dataset['fqdn-length'])*100
unknown_dataset['unique-length']=unknown_dataset['url'].apply(set).apply(len)
unknown_dataset['dot-count']= unknown_dataset['url'].str.count('\.')
unknown_dataset['hyphen-count']= unknown_dataset['url'].str.count('\-')



for index, unknown_row in unknown_dataset.iterrows():
    print(index)
    temp = tldextract.extract( unknown_row['url'] ).suffix
    subdoomain= tldextract.extract( unknown_row['url'] ).subdomain
    #unknown_dataset.at[index,'tld-suffix'] = tldextract.extract( unknown_row['url'] ).suffix
    unknown_dataset.at[index,'sub-domain-count'] = subdoomain.count('\.')+1
    
unknown_dataset = pd.merge(unknown_dataset, dataset2,how='left')


unknown_dataset.drop(
    columns=["digit-string-len", "digit-string","tld-suffix"], 
    inplace=True)

columns_titles= ['url','fqdn-length', 'fqdn-digit-percentage','unique-length',
         'dot-count','hyphen-count','sub-domain-count','rank','type']

unknown_dataset=unknown_dataset.reindex(columns=columns_titles)

unknown_dataset = unknown_dataset.fillna(0)
X_unknown = unknown_dataset.iloc[:, 1:8].values
X_unknown_features = scaler.transform(X_unknown) 

#y_unkonwn = unknown_dataset.iloc[:, 7].values 

unknown_dataset['type']=classifier.predict(X_unknown_features)






# Print results: 
#print(confusion_matrix(y_test, y_predict))
#print(classification_report(y_test, y_predict)) 








