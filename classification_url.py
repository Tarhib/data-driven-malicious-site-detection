# -*- coding: utf-8 -*-

# Feature Extraction with PCA
import numpy
from pandas import read_csv
from sklearn.decomposition import PCA



# Import libraries and classes required for this example:
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
# native
import os
import csv
import re
import tldextract

from sklearn.model_selection import cross_validate
from sklearn.metrics import make_scorer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold

from sklearn.decomposition import PCA

#feature selection
from numpy import set_printoptions
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import RFE
from newspaper import Article
from sklearn.metrics import roc_auc_score


import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import matplotlib.pyplot as plt
import re


#adding Ranodm Sample Classifcation for imabalanced data

from sklearn.ensemble import RandomForestClassifier # importing the function
from urllib.parse import urlparse
from imblearn.over_sampling import SMOTE

from sklearn.metrics import confusion_matrix,classification_report,accuracy_score,f1_score,fbeta_score,make_scorer,precision_score,recall_score 
from sklearn.svm import LinearSVC


from sklearn.linear_model import LogisticRegression



def no_of_dir(url):
    urldir = urlparse(url).path
    return urldir.count('/')
#Use of IP or not in domain
def having_ip_address(url):
    match_found = re.search(
        '(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.'
        '([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\/)|'  # IPv4
        '((0x[0-9a-fA-F]{1,2})\\.(0x[0-9a-fA-F]{1,2})\\.(0x[0-9a-fA-F]{1,2})\\.(0x[0-9a-fA-F]{1,2})\\/)' # IPv4 in hexadecimal
        '(?:[a-fA-F0-9]{1,4}:){7}[a-fA-F0-9]{1,4}', url)  # Ipv6
    if match_found:
        # print match.group()
        return -1
    else:
        # print 'No matching pattern found'
        return 1
def shortening_service(url):
    match_found = re.search('bit\.ly|goo\.gl|shorte\.st|go2l\.ink|x\.co|ow\.ly|t\.co|tinyurl|tr\.im|is\.gd|cli\.gs|'
                      'yfrog\.com|migre\.me|ff\.im|tiny\.cc|url4\.eu|twit\.ac|su\.pr|twurl\.nl|snipurl\.com|'
                      'short\.to|BudURL\.com|ping\.fm|post\.ly|Just\.as|bkite\.com|snipr\.com|fic\.kr|loopt\.us|'
                      'doiop\.com|short\.ie|kl\.am|wp\.me|rubyurl\.com|om\.ly|to\.ly|bit\.do|t\.co|lnkd\.in|'
                      'db\.tt|qr\.ae|adf\.ly|goo\.gl|bitly\.com|cur\.lv|tinyurl\.com|ow\.ly|bit\.ly|ity\.im|'
                      'q\.gs|is\.gd|po\.st|bc\.vc|twitthis\.com|u\.to|j\.mp|buzurl\.com|cutt\.us|u\.bb|yourls\.org|'
                      'x\.co|prettylinkpro\.com|scrnch\.me|filoops\.info|vzturl\.com|qr\.net|1url\.com|tweez\.me|v\.gd|'
                      'tr\.im|link\.zip\.net',
                      url)
    if match_found:
        return -1
    else:
        return 1

def feature_selection_dataset(dataset):
        dataset['tld_suffix']=''
        dataset['fqdn_length']= dataset['url'].str.len();


        dataset['digit_string']=dataset['url'].str.extract('(\d+)')
        dataset['digit_string_len']=dataset['digit_string'].str.len()
        dataset['fqdn_digit_percentage']= (dataset['digit_string_len']/dataset['fqdn_length'])*100
        dataset['unique_length']=dataset['url'].apply(set).apply(len)
        dataset['dot_count']= dataset['url'].str.count('\.')
        dataset['hyphen_count']= dataset['url'].str.count('\-')

        dataset['count?'] = dataset['url'].apply(lambda i: i.count('?'))
        dataset['count_dir'] = dataset['url'].apply(lambda i: no_of_dir(i))
        dataset['use_of_ip'] = dataset['url'].apply(lambda i: having_ip_address(i))

        dataset['short_url'] = dataset['url'].apply(lambda i: shortening_service(i))
        
        for index, row in dataset.iterrows():
            temp = tldextract.extract( row['url'] ).suffix
            subdoomain= tldextract.extract( row['url'] ).subdomain
            dataset.at[index,'tld_suffix'] = tldextract.extract( row['url'] ).suffix
            dataset.at[index,'sub_domain_count'] = subdoomain.count('\.')+1
            
        return dataset


def balance_dataset(X_train_features,y_train):
    sm = SMOTE(random_state = 2)
    X_train_res, y_train_res = sm.fit_sample(X_train_features, y_train)
    return X_train_res,y_train_res



def scaling_dataset(X_train,X_test):
    # Split dataset into random train and test subsets:
    
    # Standardize features by removing mean and scaling to unit variance:
    scaler = StandardScaler()
    scaler.fit(X_train)

    X_train_features = scaler.transform(X_train)
    X_test_features = scaler.transform(X_test) 
    return X_train_features,X_test_features

def scaling_unknonw_dataset(X_train):
    # Split dataset into random train and test subsets:
    
    # Standardize features by removing mean and scaling to unit variance:
    scaler = StandardScaler()
    scaler.fit(X_train)

    X_train_features = scaler.transform(X_train)
   
    return X_train_features


#SVM Model Dataset Run
def svm_model_run_dataset(X_train_features,y_train,X_test_features,y_test,scoring):
    svm=LinearSVC(C=0.0001)
    svm.fit(X_train_features, y_train)

    
    svm_scorar  = cross_validate(svm, X_train_features, y_train, scoring=scoring,cv=5, return_train_score=True)
    
    print(f" ACCURACY: {svm_scorar['test_accuracy'].mean()}")
    print(f" PRECISION: {svm_scorar['test_precision'].mean()}")
    print(f" RECALL: {svm_scorar['test_recall'].mean()}")
    print(f" F-1 Score: {svm_scorar['test_f1_score'].mean()}")

    # Predict y data with classifier: 
    y_predict = svm.predict(X_test_features)
    print("SVM Classification Score Analysis")
    svm_accuracy=accuracy_score(y_test, y_predict)
    print(svm_accuracy)
    print(roc_auc_score(y_test, y_predict))
    print(classification_report(y_test, y_predict,digits=4))
    print(confusion_matrix(y_test,y_predict))
    return svm    
    



#Random Forest Model Run

def random_forest_model_run_dataset(X_train_features,y_train,X_test_features,y_test,scoring):
   rf = RandomForestClassifier(n_estimators=100,criterion='entropy',random_state=42,max_depth=5) # creating an instance
   rf.fit(X_train_features,y_train)
   rf_cv_f1 = cross_validate(rf,X_test_features,y_test,cv=StratifiedKFold(n_splits=5,shuffle=True,random_state=111),scoring=scoring) 
    # fitting the model
   print(f" ACCURACY: {rf_cv_f1['test_accuracy'].mean()}")
   print(f" PRECISION: {rf_cv_f1['test_precision'].mean()}")
   print(f" RECALL: {rf_cv_f1['test_recall'].mean()}")
   print(f" F-1 Score: {rf_cv_f1['test_f1_score'].mean()}")

   y_predict = rf.predict(X_test_features) # predicting on the hold out test set
   print(classification_report(y_test,y_predict,digits=6)) 
   print(confusion_matrix(y_test,y_predict))
   return rf;


#KNN  Model Run
def knn_model_run_dataset(X_train_features,y_train,X_test_features,y_test,scoring):
   # Use the KNN classifier to fit data:
   classifier = KNeighborsClassifier(n_neighbors=5)
   classifier.fit(X_train_features, y_train) 
   # Predict y data with classifier: 
   y_predict = classifier.predict(X_test_features)
   #print("train shape: " + str(X_train.shape))
   #print("score on test: " + str(classifier.score(X_test, y_predict)))
   #print("score on train: "+ str(classifier.score(X_train, y_train)))
   print("KNN Classification Score Analysis")
   knn_accuracy= accuracy_score(y_test, y_predict)
   print(knn_accuracy)
   roc_auc_score(y_test, y_predict)
   print(roc_auc_score(y_test, y_predict))
   print(classification_report(y_test, y_predict,digits=4))
   print(confusion_matrix(y_test,y_predict))
   return classifier

#Logictic Regression  Model Run
def logistic_regression_model_run_dataset(X_train_features,y_train,X_test_features,y_test,scoring):
   clf = LogisticRegression(random_state=0)
   clf.fit(X_train_features, y_train)
   # Prediction
   y_predict = clf.predict(X_test_features)
   print("Logistic Regression Classification Score Analysis")
   knn_accuracy= accuracy_score(y_test, y_predict)
   print(knn_accuracy)
   roc_auc_score(y_test, y_predict)
   print(roc_auc_score(y_test, y_predict))
   print(classification_report(y_test, y_predict,digits=4))
   print(confusion_matrix(y_test,y_predict))
   return clf


#Unknown Dataset evalution

def unknown_datset_evalution_model(classifier_model,ranking_dataset,names_unknown):
    unknown_dataset = pd.read_csv("./mixed_domain_list.csv",names=names_unknown) 
    unknown_dataset['type']=''
    
    unknown_dataset= feature_selection_dataset(unknown_dataset)
    unknown_dataset = pd.merge(unknown_dataset, ranking_dataset,how='left')
    unknown_dataset.drop(
        columns=["digit_string_len", "digit_string","tld_suffix"], 
        inplace=True)

    

    

    columns_titles= ['url','fqdn_length'	,'fqdn_digit_percentage','unique_length'	
                'dot_count','hyphen_count',	'count?	','count_dir','use_of_ip',	
                'short_url','sub_domain_count',	'rank','type']

    unknown_dataset=unknown_dataset.reindex(columns=columns_titles)
    unknown_dataset = unknown_dataset.fillna(0)
    X_unknown = unknown_dataset.iloc[:, 1:11].values
    X_unknown_features = scaling_unknonw_dataset(X_unknown)

    #y_unkonwn = unknown_dataset.iloc[:, 7].values 

    unknown_dataset['type']=classifier_model.predict(X_unknown_features)
    return unknown_dataset



#Main Procedure strating from Here
# Assign column names to dataset:
names = ['url', 'fqdn_length', 'fqdn_digit_percentage','digit_string_len','unique_length',
         'dot_count','hyphen_count','count?','count_dir','use_of_ip','short_url',
         'tld_suffix','sub_domain_count','type']

# taking dataset from benign dataset
dataset = pd.read_csv("./benign_domain_list.csv",names=names) 
dataset['type']=0

malicious_dataset = pd.read_csv("./Malicious_URLs-dataset-for-research.csv",names=names) 
malicious_dataset['type']=1

dataset = pd.concat([dataset, malicious_dataset], ignore_index=True)

dataset= feature_selection_dataset(dataset)



ranking_dataset_names = ['rank', 'url']
ranking_dataset = pd.read_csv("./tranco_full_list_for_ranking.csv",names=ranking_dataset_names) 

dataset = pd.merge(dataset, ranking_dataset,how='left')



maclious_dataset= dataset.loc[dataset['type'] == 1]

top_domain_maclious = maclious_dataset['tld_suffix'].value_counts().nlargest(10)

benign_dataset_ = dataset.loc[dataset['type'] == 0]
top_domain_benign = benign_dataset_['tld_suffix'].value_counts().nlargest(10)


plt.figure(figsize=(15,5))
sns.countplot(x='tld_suffix',data=dataset)
plt.title("Count Of URLs",fontsize=20)
plt.xlabel("Type Of URLs",fontsize=18)
plt.ylabel("Number Of URLs",fontsize=18)



dataset.drop(
    columns=["digit_string_len", "digit_string","tld_suffix"], 
    inplace=True)



dataset.to_csv('features_benign.csv', header=True, index=False)
columns_titles= ['url','fqdn_length'	,'fqdn_digit_percentage','unique_length'	
            'dot_count','hyphen_count',	'count?	','count_dir','use_of_ip',	
            'short_url','sub_domain_count',	'rank','type']

dataset=dataset.reindex(columns=columns_titles)
dataset = dataset.fillna(0)

dataset.head() 

# Assign values to the X and y variables:
X = dataset.iloc[:, 1:11].values
y = dataset.iloc[:, 11].values 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30) 

X_train_features,X_test_features = scaling_dataset(X_train,X_test)


# creating a dictionary to evaluate metric over stratified k-fold cv
scoring = {'accuracy' : make_scorer(accuracy_score), 
           'precision' : make_scorer(precision_score),
           'recall' : make_scorer(recall_score), 
           'f1_score' : make_scorer(f1_score)}
#balanced_dataset
X_train_res,y_train_res =balance_dataset(X_train_features,y_train)


#imbalanced-data using svm
print("SVM Imabalanced Score Analysis:")
svm =svm_model_run_dataset(X_train_features, y_train, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(svm, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_svm_output.csv', header=True, index=False)


#balanced-data using svm
print("SVM Balanced Score Analysis:")
svm =svm_model_run_dataset(X_train_res, y_train_res, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(svm, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_balanced_svm_output.csv', header=True, index=False)


#imbalanced-data using KNN
print("KNN Imabalanced Score Analysis:")
knn =knn_model_run_dataset(X_train_features, y_train, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(knn, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_knn_output.csv', header=True, index=False)

#balanced-data using KNN
print("Knn Balanced Score Analysis:")
knn =knn_model_run_dataset(X_train_res, y_train_res, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(knn, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_balanced_knn_output.csv', header=True, index=False)




#imbalanced-data
print("Random Forest Imabalanced Score Analysis:")
random_forest =random_forest_model_run_dataset(X_train_features, y_train, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(random_forest, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_random_forest_output.csv', header=True, index=False)

#balanced-data
print("Random Forest Balanced Score Analysis:")
random_forest =random_forest_model_run_dataset(X_train_res, y_train_res, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(random_forest, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_random_forest_balanced_output.csv', header=True, index=False)


# LogisticRegression
print("Logistic Regression Imabalanced Score Analysis:")
logistic_regression =logistic_regression_model_run_dataset(X_train_features, y_train, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(logistic_regression, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_logistic_regression_output.csv', header=True, index=False)

# LogisticRegression
print("Logistic Regression Balanced Score Analysis:")
logistic_regression =logistic_regression_model_run_dataset(X_train_res, y_train_res, X_test_features, y_test, scoring)
unknown_dataset = unknown_datset_evalution_model(logistic_regression, ranking_dataset, names)
unknown_dataset.to_csv('unknown_dataset_logistic_regression_output.csv', header=True, index=False)














  


#SVM Classification

























# Print results: 
#print(confusion_matrix(y_test, y_predict))
#print(classification_report(y_test, y_predict)) 








